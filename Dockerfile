FROM centos:7

LABEL maintainer="rcnetto@gmail.com"

#Instala dependências
RUN yum -y install wget sudo
RUN curl --silent --location https://rpm.nodesource.com/setup_10.x | sudo bash -
RUN yum-config-manager --save --setopt=nodesource.skip_if_unavailable=true && \
 yum-config-manager --save --setopt=nodesource-source.skip_if_unavailable=true
RUN yum -y install nodejs
RUN curl -O http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm && \
 rpm -ivh epel-release-latest-7.noarch.rpm && \
 rm -rf epel-release-latest-7.noarch.rpm
RUN yum -y install nano ant systemd zip make procps automake bison flex gcc-c++ git gperf graphviz junit4 libcap-devel cppunit-devel cppunit-doc mesa-libEGL fontconfig-devel mesa-libGL-devel gtk3-devel gtk2-devel krb5-devel libpcap libpcap-devel libtool pam-devel libxml2 libXrandr-devel libXrender-devel libxslt-devel libXt-devel m4 nasm openssl openssl-devel pkgconfig python-devel uuid util-linux libcap python-lxml lsof doxygen cups-devel gstreamer1-devel \
 ccache CUnit CUnit-devel python2-polib python34-devel java-1.8.0-openjdk gstreamer-plugins-base-devel \
 redhat-lsb-core which \
 libpng12 libpng12-devel \
 yum groupinstall -y 'Development Tools' && \
 yum-builddep -y libreoffice && \
 yum clean all && \
 rm -rf /var/cache/yum

# Clona o repositório do LibreOffice
#user lool
RUN getent passwd lool || (useradd lool -G wheel)
RUN chown lool:lool /home/lool -R
RUN chown lool:lool /opt -R
USER lool
RUN mkdir /opt/libreoffice
COPY /LibreOfficeOnlineMin.conf /opt/
WORKDIR /opt/libreoffice
RUN git clone --depth 1 --single-branch --branch cd-5.3-49 https://github.com/LibreOffice/core.git /opt/libreoffice && \
 git config --file=.gitmodules submodule.dictionaries.url http://anongit.freedesktop.org/git/libreoffice/dictionaries.git &&\
 git config --file=.gitmodules submodule.helpcontent2.url http://anongit.freedesktop.org/git/libreoffice/help.git &&\
 git config --file=.gitmodules submodule.translations.url http://anongit.freedesktop.org/git/libreoffice/translations.git &&\
 git submodule sync && \
 git submodule update --init --recursive translations && \
 mv /opt/LibreOfficeOnlineMin.conf /opt/libreoffice/distro-configs
RUN ./autogen.sh --with-distro=LibreOfficeOnlineMin && \
 make && \
 make build-l10n-only && \
 ccache -C && \
 mkdir /opt/libreoffice-instdir && \
 mv /opt/libreoffice/instdir /opt/libreoffice-instdir && \
 rm -rf /opt/libreoffice
#Constrói POCO
USER root
WORKDIR /opt
RUN curl -O https://pocoproject.org/releases/poco-1.7.8/poco-1.7.8-all.tar.gz && \
 tar xf /opt/poco-1.7.8-all.tar.gz
WORKDIR /opt/poco-1.7.8-all
RUN ./configure --omit=Data/ODBC,Data/MySQL && \
 make clean && \
 make -j8 && \
 make install && \
 ccache -C && \
 rm -rf /opt/poco-1.7.8-all.tar.gz

#Constrói LOOL
# Clona o repositório do LibreOffice Online
RUN mkdir /opt/online
WORKDIR /opt/online
RUN git clone --depth 1 --single-branch --branch cd-3.2.2-8 https://github.com/LibreOffice/online.git /opt/online
RUN npm install -g npm && \
 npm install -g jake && \
 ./autogen.sh && \
 ./configure --enable-silent-rules --with-lokit-path=./bundled/include --with-lo-path=/opt/libreoffice-instdir --with-max-connections=300 --with-max-documents=150 --with-poco-includes=/usr/local/include --with-poco-libs=/usr/local/lib && \
 make

# envio para o docker
#docker build -t lool-build-deps .
#docker login --username=rcnetto
#docker tag 25f88441386c rcnetto/lool-build-deps:cd-3.2.2-8-built-pre-install
#docker push rcnetto/lool-build-deps:cd-3.2.2-8-built-pre-install